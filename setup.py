#!/usr/bin/env python
from setuptools import setup, find_packages

entry_points = '''
[pygments.lexers]
lncf=pymdown_lexers:LanguageConfigLexer
'''

setup(
    name='pygments-custom-lexer',
    version='1.0.2',
    packages=find_packages(),
    entry_points=entry_points,
    install_requires=[
        'Pygments>=2.0.1'
    ],
    zip_safe=True,
)
