pygments custom lexer
==================

A package created to add additional lexers for use in Pygments.  This package was created to be used in the [LNCF User Guide](https://rymiel.gitlab.io/lncf/docs/).

# Overview
The purpose of this package was to natively add the lexers to Pygments via a plugin.  This was so that PyMdown could use 3rd party and native lexers without having to directly modify a Pygments installation.

The idea was to add the custom Pygments lexers here, and build a package that specifies the correct entry points.  When it is installed, the custom lexers can be used as if they were native.

I don't actually imagine many lexers will be added to this, but they are not needed to use PyMdown.

Cloned from https://github.com/facelessuser/pymdown-lexers
