"""A Pygments lexer for lncf."""
from pygments.lexers import LuaLexer

__all__ = ("LanguageConfigLexer",)


class LanguageConfigLexer(LuaLexer):
    name = 'Language Config'
    aliases = ['lncf']
    filenames = ['*.lncf.lua']
    mimetypes = [*LuaLexer.mimetypes, 'text/lncf-lua']
    def __init__(self, **options):
        LuaLexer.__init__(self, **options)
        self._functions |= {"flow", "fn"}
